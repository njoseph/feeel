Feeel
=====

Feeel is an open-source Android app for doing simple at-home exercises. While it currently only holds the [scientific 7-minute workout regime](https://well.blogs.nytimes.com/2013/05/09/the-scientific-7-minute-workout/), the hope is to expand the amount of available exercises with the help of the community and to allow creating custom regimes, with an assortment of handful time-proven regimes built-in to start with.

To get a general idea of the app, see this video:
![YouTube video introduction](https://youtu.be/h1-HracNEWE)

Contribute
====
ANYONE can contribute, no special skills required. Message me on [Gitter](https://gitter.im/feeel_app) if you'd like to help.

Donate
====
If you'd prefer to donate instead, you can donate through these:
- **[Liberapay](https://liberapay.com/Feeel/)**
- [Bountysource](https://salt.bountysource.com/teams/feeel)

Install
====

[<img src="https://f-droid.org/badge/get-it-on.png"
      alt="Get it on F-Droid"
      height="80">](https://f-droid.org/packages/com.enjoyingfoss.feeel/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
      alt="Get it on Google Play"
      height="80">](https://play.google.com/store/apps/details?id=com.enjoyingfoss.feeel)
